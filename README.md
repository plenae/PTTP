# PTTP - Public Transport Ticket Parser

PTTP parses emails with public transport ticket info to CSV.

Today it does so for Dutch emails from the Belgian rail provider NMBS.

I need to report in detail on my ticket purchases, but I buy them through the
NMBS app. As a result, my emails are my only trace. PTTP parses dozens of such
emails to a detailed CSV file.

If you are interested in using this for another purpose, please let me know and
we can consider to make parsing logic pluggable for different public transport
providers.

## Installation

1. Clone this repository.

2. Install python dependencies using pip:
   `pip install eml-parser html2text`

## Usage

```bash
$ python ./pttp_nmbs.py -h
usage: pttp_nmbs.py [-h] [--outfile OUTFILE] [--verbose] ticket [ticket ...]

Parse ticket details from NMBS mails to CSV.

positional arguments:
  ticket                an EML format NMBS e-mail.

options:
  -h, --help            show this help message and exit
  --outfile OUTFILE, -o OUTFILE
                        File to output CSV to. Default: sys.stdout.
  --verbose, -v         Increase verbosity to debug.
```

## Examples

In the examples project dir are ticket mails. They convert to CSV below:

| mail\_date          | mail\_to               | amount\_tickets | ticket\_type               | ticket\_class | ticket\_from | ticket\_to              | ticket\_date        | ticket\_single\_return | price | eml\_file                                                                                                               |
| ------------------- | ---------------------- | --------------- | -------------------------- | ------------- | ------------ | ----------------------- | ------------------- | ---------------------- | ----- | ----------------------------------------------------------------------------------------------------------------------- |
| 04/01/2022 09:37:37 | <my_email@provider.be> | 1               | LOCAL MULTI                | 2             | MY HOME      | MY-NEAREST-CITYS        | 04/01/2022 00:00:00 | SINGLE                 | 0.0   | examples/NMBS Mobile Ticket NL - "DO NOT REPLY: <no-reply@nmbs.be>" <documentpresentment@ypto.be> - 2022-01-04 0937.eml |
| 31/01/2022 08:45:37 | <my_email@provider.be> | 1               | STANDARD MULTI             | 2             | MY HOME      | SOME-OTHER-FARTHER-CITY | 31/01/2022 00:00:00 | SINGLE                 | 0.0   | examples/NMBS Mobile Ticket NL - "DO NOT REPLY: <no-reply@nmbs.be>" <documentpresentment@ypto.be> - 2022-01-31 0845.eml |
| 01/02/2022 08:55:32 | <my_email@provider.be> | 1               | STANDARD TICKET            | SECOND        | MY HOME      | SOME-FARTHER-CITY       | 01/02/2022 00:00:00 | ROUNDTRIP              | 15.4  | examples/NMBS Mobile Ticket NL - "DO NOT REPLY: <no-reply@nmbs.be>" <documentpresentment@ypto.be> - 2022-02-01 0855.eml |
| 12/03/2022 13:54:17 | <my_email@provider.be> | 1               | LOCAL MULTI (KEY CARD)     | SECOND        |              |                         | 12/03/2022 13:54:17 |                        | 27.0  | examples/NMBS Mobile Ticket NL - "DO NOT REPLY: <no-reply@nmbs.be>" <documentpresentment@ypto.be> - 2022-03-12 1354.eml |
| 28/03/2022 08:48:39 | <my_email@provider.be> | 1               | STANDARD MULTI (RAIL PASS) | SECOND        |              |                         | 28/03/2022 08:48:39 |                        | 83.0  | examples/NMBS Mobile Ticket NL - "DO NOT REPLY: <no-reply@nmbs.be>" <documentpresentment@ypto.be> - 2022-03-28 0848.eml |
| 16/08/2022 12:39:50 | <my_email@provider.be> | 1               | DUO TICKET                 | SECOND        | MY HOME      | SOME DEST               | 16/08/2022 00:00:00 | ROUNDTRIP              | 19.8  | examples/NMBS Mobile Ticket NL - "DO NOT REPLY: <no-reply@nmbs.be>" <documentpresentment@ypto.be> - 2022-08-16 1239.eml |
| 07/12/2022 07:57:14 | <my_email@provider.be> | 1               | GROTE GEZINNEN (CODE 040)  | SECOND        | MY HOME      | SOME CITY               | 07/12/2022 00:00:00 | ROUNDTRIP              | 12.4  | examples/NMBS Mobile Ticket NL - "DO NOT REPLY: <no-reply@nmbs.be>" <documentpresentment@ypto.be> - 2022-12-07 0757.eml |

### Sorting tickets per payer and convert to PDF

I use the CSV output of PTTP to sort the tickets per paying party. Different
paying parties should reimburse the tickets to me, so I need to sort them. Next
I need to create PDF's from each EML as proof of my purchase to get my costs
reimbursed.

#### Sorting

1. I add a column to the CSV and manually assign them to a paying party. Using
   LibreOffice Calc or Gnumeric here.

2. Next I need to generate PDF's for each paying party for each of the EML's.
   So, based on the filtering column in the CSV, I create a text file per paying
   party with just the filenames of the EML's for that paying party.

3. I copy/link the EML files in a dir per paying party using bash. Example for
   a paying party "vc" here below:

```bash
while read F ; do
    ln "$F" vc
done < vc_files # vc_files contains just filenames, per line, of eml files to ln to directory "vc"
```

#### Convert tickets to PDF

I convert all EML's per paying party directory to PDF's using <https://github.com/nickrussler/email-to-pdf-converter>.
In a single command like

```bash
for f in *.eml ; do java -jar $HOME/opt/emailconverter-2.5.3-all.jar "$f" ; done
```

## License

This project is released under a GPLv3 license. See COPYING for the full
license text.
