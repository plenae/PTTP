"""Search the bodies of NMBS/SNCB emails for ticket info and output to CSV."""

#    pttp_nmbs.py parses Belgian Railway ticket emails to CSV.
#    Copyright (C) 2022 Pieter Lenaerts <pieter.aj.lenaerts@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import re
import logging
import datetime
from io import BufferedReader
from locale import atof, setlocale, LC_NUMERIC
from typing import List, TextIO
from csv import DictWriter
import sys
from dataclasses import dataclass, asdict, fields

import eml_parser
import html2text


@dataclass
class Ticket:
    """A train ticket with very basic attributes."""

    # pylint: disable=too-many-instance-attributes
    mail_date: str
    mail_to: str
    amount_tickets: int
    ticket_type: str
    ticket_class: str
    ticket_from: str
    ticket_to: str
    ticket_date: str
    ticket_single_return: str
    price: float
    eml_file: str


class UnsupportedTicketTypeException(Exception):
    """Raised when parsing a ticket fails at matching a supported pattern."""

    pass


STATION_REGEX = r'[\S \n]+'

RE_M_TICKET = r'Je.hebt.(?P<aantal>\d+).rit\(ten\).van.jouw.' \
              r'(?P<type>(LOCAL|STANDARD).MULTI).geactiveerd,.geldig.in.' \
              r'(?P<klasse>\d)e.klas.van.(?P<van>' + STATION_REGEX + \
              r').naar.(?P<naar>' + STATION_REGEX + \
              r').op.datum.van.(?P<datum>[\d/]+).'

RE_STD = r'Artikel.van.het.type.(?P<type>STANDARD.TICKET).' \
         r'(?P<single_return>SINGLE|ROUNDTRIP),.' \
         r'geldig.in.(?P<klasse>SECOND|FIRST).klas.van.(?P<van>' + \
         STATION_REGEX + r').naar.(?P<naar>' + STATION_REGEX + r').op.' \
         r'datum.van.(?P<datum>[\d/]+).' \
         r'voor.een.totaal.prijs.van.(?P<prijs>[\d\,]+).EUR\.'

RE_DUO = r'Artikel.van.het.type.(?P<type>DUO.TICKET).' \
         r'(?P<single_return>SINGLE|ROUNDTRIP),.' \
         r'geldig.in.(?P<klasse>SECOND|FIRST).klas.van.(?P<van>' + \
         STATION_REGEX + r').naar.(?P<naar>' + STATION_REGEX + r').op.' \
         r'datum.van.(?P<datum>[\d/]+).' \
         r'voor.een.totaal.prijs.van.(?P<prijs>[\d\,]+).EUR\.'

RE_MULTI = r'Artikel.van.het.type.(?P<type>(STANDARD|LOCAL).MULTI.' \
           r'\((RAIL.PASS|KEY.CARD)\)).SINGLE,.' \
           r'geldig.in.(?P<klasse>SECOND|FIRST).klas.' \
           r'voor.een.totaal.prijs.van.(?P<prijs>[\d\,]+).EUR\.'

RE_040 = r'Artikel.van.het.type.(?P<type>GROTE.GEZINNEN.\(CODE.040\)).' \
         r'(?P<single_return>SINGLE|ROUNDTRIP),.' \
         r'geldig.in.(?P<klasse>SECOND|FIRST).klas.van.(?P<van>' + \
         STATION_REGEX + r').naar.(?P<naar>' + STATION_REGEX + r').op.' \
         r'datum.van.(?P<datum>[\d/]+).' \
         r'voor.een.totaal.prijs.van.(?P<prijs>[\d\,]+).EUR\.'

# LibreOffice Calc seems to like this strf best
DATE_OUT_FORMAT = '%d/%m/%Y %H:%M:%S'


def parse_raw_eml_from_file(raw_eml_file_br: BufferedReader):
    """Return parsed eml file as dict.

    Input: io.BuferedReader for raw EML file.
    Return: EML file as dict.
    """
    raw_email = raw_eml_file_br.read()

    emlparser = eml_parser.EmlParser(include_raw_body=True)
    parsed_eml = emlparser.decode_email_bytes(raw_email)
    return parsed_eml


def parse_and_format_date(datestring: str) -> str:
    """Parse a date string and return a formatted datestring.

    Parse from format '%d/%m/%Y' and return formatted as
    DATE_OUT_FORMAT global const.
    """
    parsed_date = datetime.datetime.strptime(datestring, '%d/%m/%Y')
    return parsed_date.strftime(DATE_OUT_FORMAT)
    # return parsed_date


def get_ticket_from_eml(raw_eml_file_br: BufferedReader) -> Ticket:
    """Parse a ticket eml and return a Ticket object.

    Input: io.BufferedReader for raw EML file.
    Return: a Ticket oject parsed from the ticket EML.
    """
    parsed_eml = parse_raw_eml_from_file(raw_eml_file_br)
    raw_eml_file_name = raw_eml_file_br.name

    # Get the mail date and to from the eml headers,
    # not from the eml body as text.
    # Assume that NMBS sends to only one recipient.
    mail_date = parsed_eml.get('header').get('date')
    mail_to = parsed_eml.get('header').get('to')[0]

    # There can be multiple bodies in an eml.
    # We iterate below for the first non empty body.
    bodies = parsed_eml.get('body')

    # Convert the HTML bodies to text to easily search them.
    h2t = html2text.HTML2Text()
    # Ignore converting links from HTML
    h2t.ignore_links = True
    body_text = ''  # pylint: disable=C0103
    i = 0
    while not body_text and i < len(bodies):
        body_text = h2t.handle(bodies[i].get('content')).strip()
        i = i + 1

    # Search body for the patterns and create a Ticket object
    # based on the type.
    m_m_ticket = re.search(RE_M_TICKET, body_text, re.DOTALL)

    m_std = re.search(RE_STD, body_text, re.DOTALL)

    m_duo = re.search(RE_DUO, body_text, re.DOTALL)

    m_multi = re.search(RE_MULTI, body_text, re.DOTALL)

    m_040 = re.search(RE_040, body_text, re.DOTALL)

    new_ticket = Ticket("", "", 0, "", "", "", "", "", "", 0.0, "")

    if m_m_ticket:
        new_ticket = Ticket(mail_date.strftime(DATE_OUT_FORMAT), mail_to,
                            int(m_m_ticket.group('aantal')),
                            m_m_ticket.group(
                                'type'), m_m_ticket.group('klasse'),
                            m_m_ticket.group('van').replace('\n', ' '),
                            m_m_ticket.group('naar').replace('\n', ' '),
                            parse_and_format_date(m_m_ticket.group('datum')),
                            'SINGLE',
                            0.0, raw_eml_file_name)

    elif m_std:
        new_ticket = Ticket(mail_date.strftime(DATE_OUT_FORMAT), mail_to, 1,
                            m_std.group('type'),
                            m_std.group('klasse'),
                            m_std.group('van').replace('\n', ' '),
                            m_std.group('naar').replace('\n', ' '),
                            parse_and_format_date(m_std.group('datum')),
                            m_std.group('single_return'),
                            atof(m_std.group('prijs')), raw_eml_file_name)

    elif m_duo:
        new_ticket = Ticket(mail_date.strftime(DATE_OUT_FORMAT), mail_to, 1,
                            m_duo.group('type'),
                            m_duo.group('klasse'),
                            m_duo.group('van').replace('\n', ' '),
                            m_duo.group('naar').replace('\n', ' '),
                            parse_and_format_date(m_duo.group('datum')),
                            m_duo.group('single_return'),
                            atof(m_duo.group('prijs')), raw_eml_file_name)

    elif m_040:
        new_ticket = Ticket(mail_date.strftime(DATE_OUT_FORMAT), mail_to, 1,
                            m_040.group('type'),
                            m_040.group('klasse'),
                            m_040.group('van').replace('\n', ' '),
                            m_040.group('naar').replace('\n', ' '),
                            parse_and_format_date(m_040.group('datum')),
                            m_040.group('single_return'),
                            atof(m_040.group('prijs')), raw_eml_file_name)

    elif m_multi:
        new_ticket = Ticket(mail_date.strftime(DATE_OUT_FORMAT), mail_to, 1,
                            m_multi.group('type'),
                            m_multi.group('klasse'), '', '',
                            mail_date.strftime(DATE_OUT_FORMAT), '',
                            atof(m_multi.group('prijs')), raw_eml_file_name)
    else:
        # None of our patterns matched. We need more patterns or we have
        # another mail alltogether.
        logging.error('Could not match any pattern to file: %s',
                      raw_eml_file_name)
        logging.error('Run with -v to see html2text\'ed text.')
        logging.debug(body_text)
        raise UnsupportedTicketTypeException()
    return new_ticket


def write_tickets_to_csv(ticketlist: list[Ticket], out: TextIO):
    """Write ticket info to CSV output.

    Input:
        * ticketlist: list[Ticket] to output to CSV.
        * out: TextIO object to output CSV to. sys.stdout for example.
    """
    # Get a list of fieldnames from the dataclass Ticket class.
    # Init a DictWriter with the list, write header and all rows.
    fieldnames = [f.name for f in fields(Ticket)]
    writer = DictWriter(out, fieldnames=fieldnames)
    writer.writeheader()
    for ticket in ticketlist:
        if ticket.mail_date:
            writer.writerow(asdict(ticket))


if __name__ == '__main__':
    # All e-mails I get are Dutch, Flemish.
    # We need to set locale for casting string to float using atof() later.
    setlocale(LC_NUMERIC, 'nl_BE.UTF-8')

    parser = argparse.ArgumentParser(
            description='Parse ticket details from NMBS mails to CSV.')
    # Using argparse.FileType args to use argparse checks on
    # read/writability from arguments.
    parser.add_argument('mail', metavar='ticket',
                        type=argparse.FileType('rb'), nargs='+',
                        help='an EML format NMBS e-mail.')
    parser.add_argument('--outfile', '-o', type=argparse.FileType('w'),
                        default=sys.stdout,
                        help='File to output CSV to. Default: sys.stdout.')
    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help='Increase verbosity to debug.')
    args = parser.parse_args()

    # Set the log level
    if args.verbose > 0:
        LOG_LVL = logging.DEBUG
    else:
        LOG_LVL = logging.WARNING

    logging.basicConfig(level=LOG_LVL)

    # Iterate over all eml files in args, put them in a list,
    # write the list to CSV
    tickets: List[Ticket] = []

    for eml_file in args.mail:
        # Multi-thread here? I guess that's not worth the effort...
        try:
            tickets.append(get_ticket_from_eml(eml_file))
        except UnsupportedTicketTypeException:
            pass

    write_tickets_to_csv(tickets, args.outfile)
